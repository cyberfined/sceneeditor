#include "scene_editor.hpp"

#include <GLXW/glxw.h>

SceneEditor::SceneEditor(BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> &builder): Gtk::GLArea(cobject) {
    this->signal_render().connect(sigc::mem_fun(*this, &SceneEditor::render), false);
}

bool SceneEditor::render(const Glib::RefPtr<Gdk::GLContext> &ctx) {
    glClearColor(1.f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glFlush();
    return true;
}
