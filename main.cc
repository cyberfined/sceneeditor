#include <iostream>
#include <fstream>
#include <gtkmm.h>

#include <GLXW/glxw.h>
#include "scene_editor.hpp"
#include "raytracer_preview.hpp"

static inline Glib::RefPtr<Gtk::Builder> builder_create_from_file(const std::string &filename) {
#ifndef _MSYS2
    return Gtk::Builder::create_from_file(filename);
#else
    try {
        std::ifstream stream(filename);
        stream.seekg(0, stream.end);
        int length = stream.tellg();
        stream.seekg(0, stream.beg);
        char* buf = new char[length];
        stream.read(buf, length);
        stream.close();

        auto builder = Gtk::Builder::create();
        builder->add_from_string(buf, length);
        delete [] buf;
        return builder;
    } catch(std::exception&) {
        std::cerr << "Failed to open file " << filename << std::endl;
        return Glib::RefPtr<Gtk::Builder>(NULL);
    }
#endif
}

class MainWindow: public Gtk::Window {
private:
    Glib::RefPtr<Gtk::Builder> builder;
    SceneEditor *scene_editor;
    RaytracerPreview *raytracer_preview;

public:
    MainWindow(BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> &builder): Gtk::Window(cobject) {
        this->builder = builder;

        builder->get_widget_derived("scene_editor", scene_editor);
        builder->get_widget_derived("raytracer_preview", raytracer_preview);
    }

    ~MainWindow() {
        delete scene_editor;
        delete raytracer_preview;
    }
};

int main(int argc, char **argv) {
    Gtk::Main app(argc, argv);
    if(glxwInit() != 0) {
        std::cerr << "Failed to initialize OpenGL" << std::endl;
        return 1;
    }

    auto builder = builder_create_from_file("sceneeditor.glade");
    if(!builder)
        return 1;

    MainWindow *window;
    builder->get_widget_derived("main_wnd", window);
    app.run(*window);
    delete window;
}
