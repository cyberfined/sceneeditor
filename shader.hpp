#pragma once
#include <cstddef>

#include <GLXW/glxw.h>

class Shader {
private:
    GLuint shader;
public:
    Shader(GLenum shader_type, const char *source);
    GLuint get_shader();
    ~Shader();
};

class ShaderProgram {
private:
    GLuint program;
public:
    ShaderProgram(const char *vertex_src, const char *fragment_src);
    void use_program();
    ~ShaderProgram();
};
