#pragma once
#include <gtkmm.h>

class RaytracerPreview: public Gtk::GLArea {
private:
public:
    RaytracerPreview(BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> &builder);
};
