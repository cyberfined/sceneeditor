#pragma once

#include <cstddef>
#include <gtkmm.h>
#include <GLXW/glxw.h>

class Color {
public:
    float r, g, b;
};

class SceneEditor: public Gtk::GLArea {
private:
    bool render(const Glib::RefPtr<Gdk::GLContext> &ctx);
public:
    SceneEditor(BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> &builder);
};

class Drawable {
public:
    Drawable(float *vertices, std::size_t num_vertices, GLenum mode);
};
