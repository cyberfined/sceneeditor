#include "shader.hpp"
#include <iostream>
#include <stdexcept>
#include <memory>

#include <GLXW/glxw.h>

Shader::Shader(GLenum shader_type, const char *source) {
    this->shader = glCreateShader(shader_type);
    glShaderSource(this->shader, 1, &source, NULL);
    glCompileShader(this->shader);

    GLint compile_status;
    glGetShaderiv(this->shader, GL_COMPILE_STATUS, &compile_status);
    if(compile_status != GL_TRUE) {
        GLsizei msg_len;
        GLchar msg[512];
        glGetShaderInfoLog(this->shader, sizeof(msg), &msg_len, msg);
        throw std::runtime_error(std::string("Shader compile error: ") + std::string(msg, msg_len));
    }
}

GLuint Shader::get_shader() {
    return this->shader;
}

Shader::~Shader() {
    glDeleteShader(this->shader);
}

ShaderProgram::ShaderProgram(const char *vertex_src, const char *fragment_src) {
    try {
        std::unique_ptr<Shader> vertex(new Shader(GL_VERTEX_SHADER, vertex_src));
        std::unique_ptr<Shader> fragment(new Shader(GL_FRAGMENT_SHADER, fragment_src));

        this->program = glCreateProgram();
        glAttachShader(this->program, vertex->get_shader());
        glAttachShader(this->program, fragment->get_shader());
        glLinkProgram(this->program);

        GLint link_status;
        glGetProgramiv(this->program, GL_LINK_STATUS, &link_status);
        if(link_status != GL_TRUE) {
            GLsizei msg_len;
            GLchar msg[512];
            glGetProgramInfoLog(this->program, sizeof(msg), &msg_len, msg);
            throw std::runtime_error(std::string("Program link error: ") + std::string(msg, msg_len));
        }
    } catch(const std::runtime_error &e) {
        throw;
    }
}

void ShaderProgram::use_program() {
    glUseProgram(this->program);
}

ShaderProgram::~ShaderProgram() {
    glDeleteProgram(this->program);
}
